import React, { Component, Fragment } from 'react';
import { inject, observer } from 'mobx-react';

import Hand from '../../components/Hand/Hand';
import Interface from '../../components/Interface/Interface';

@inject('TableStore')
@observer
class Table extends Component {
    render() {
        const {
            computerHand,
            message,
            deal,
            hit,
            stand,
            computerScore,
            playerScore,
            playerHand
        } = this.props.TableStore;
        return (
            <Fragment>
                <Hand cards={computerHand} />
                <Interface
                    message={message}
                    deal={deal}
                    hit={hit}
                    stand={stand}
                    computerScore={computerScore}
                    playerScore={playerScore}
                />
                <Hand cards={playerHand} />
            </Fragment>
        );
    }
}
export default Table;
