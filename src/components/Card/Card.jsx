import React from 'react';
import PropTypes from 'prop-types';

import { FOLDED_CARD } from '../../utils/constants';
import { ThemeContext } from '../../context/ThemeContext';

Card.propTypes = {
    face: PropTypes.string.isRequired
};

function Card({ face }) {
    return (
        <div className="card">
            <ThemeContext.Consumer>
                {
                    ({ theme, switchTheme }) =>
                    face === FOLDED_CARD ? (
                        <img src={`images/${theme}.png`} alt="card" style={styles.Pointer} onClick={switchTheme} />
                    ) : (
                        <img src={`images/${face}.png`} alt="card" />
                    )
                }
            </ThemeContext.Consumer>
        </div>
    );
}
export default Card;

const styles = {
    Pointer: {
        cursor: 'pointer'
    }
};
