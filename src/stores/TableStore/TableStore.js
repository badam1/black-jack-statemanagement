import { observable, action, computed } from 'mobx';
import shuffle from 'lodash/shuffle';

import fullDeck from '../../utils/fullDeck';
import countScore from '../../utils/countScore';
import {
    FOLDED_CARD,
    DEAL_MESSAGE,
    PLAYING,
    HIT_OR_STAND,
    GAME_OVER,
    OVERDRAWN,
    DEALER_HIT_LIMIT,
    BLACK_JACK_SCORE,
    COMPUTER_WINS,
    PLAYER_WINS
} from '../../utils/constants';

class TableStore {
    @observable deck = shuffle([...fullDeck]);
    @observable computerHand = [FOLDED_CARD, FOLDED_CARD];
    @observable playerHand = [FOLDED_CARD, FOLDED_CARD];
    @observable message = DEAL_MESSAGE;
    @observable status = PLAYING;

    @action
    deal = () => {
        this.deck = shuffle([...fullDeck]);
        this.computerHand = [FOLDED_CARD];
        this.playerHand = [];
        this.playerHand.push(this.deck.pop());
        this.playerHand.push(this.deck.pop());
        this.deck.pop();
        this.computerHand.push(this.deck.pop());
        this.status = PLAYING;
        this.message = HIT_OR_STAND;
    };

    @action
    hit = () => {
        if (this.status === GAME_OVER) return;
        this.deck = shuffle([...this.deck]);
        this.playerHand.push(this.deck.pop());
        if (this.playerScore > 21) {
            this.message = OVERDRAWN;
            this.status = GAME_OVER;
        }
    };

    @action
    stand = () => {
        if (this.status === GAME_OVER) return;
        this.computerHand.shift();
        this.deck = shuffle([...this.deck]);
        this.computerHand.unshift(this.deck.pop());
        while (this.computerScore < this.playerScore || this.computerScore < DEALER_HIT_LIMIT) {
            this.computerHand.push(this.deck.pop());
        }
        this.status = GAME_OVER;
        this.message = this.computerScore > BLACK_JACK_SCORE ? PLAYER_WINS : COMPUTER_WINS;
    };

    @computed
    get playerScore() {
        return countScore(this.playerHand);
    }

    @computed
    get computerScore() {
        return countScore(this.computerHand);
    }
}
export default new TableStore();
