import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';

import TableStore from './stores/TableStore/TableStore';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const app = (
    <Provider TableStore={TableStore}>
        <App />
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
