import React, { Component } from 'react';
import './App.css';

import Table from './containers/Table/Table';
import ThemeProvider from './context/ThemeContext';

class App extends Component {
    render() {
        return (
            <div className="App">
                <ThemeProvider>
                    <Table />
                </ThemeProvider>
            </div>
        );
    }
}

export default App;
